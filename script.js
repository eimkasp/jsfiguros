var figuros = [
    {ilgis: 2, plotis: 5, aukstis: 3},
    {ilgis: 3, plotis: 2, aukstis: 6},
    {ilgis: 1, plotis: 5, aukstis: 5}
];


var spausdintiButton = document.getElementById("spausdinti");
var duomenysTable = document.querySelector("#duomenys tbody");

var maxButton = document.getElementById("rastiMax");
var turisTh = document.getElementById("turisTh");
var maxFiguraIndex;

maxButton.addEventListener("click", function() {
    skaiciuotiTuri();
    rastiDidziausiaFigura();
    spausdintiDuomenis(true);
    // var didziausiasRow =  document.querySelector("#duomenys tbody tr:nth-child(" + maxFigura + ")");
    // console.log(didziausiasRow);
    // didziausiasRow.classList.add("red");
});

function rastiDidziausiaFigura() {

    var max = 0;
    var maxIndex = 0;
    // Ciklas kuris eina per visas figuras masyve
    for(let i = 0; i < figuros.length; i++) {
        // Tikrinu ar einamoji figura yra didesne uz diziausia rasta iki siol
        if(figuros[i].turis > max) {
            max = figuros[i].turis;
            maxIndex = i;
        }
    }
    /// Prisikiriu globaliam kintamajam rasta reiksme
    maxFiguraIndex = maxIndex;
}

function skaiciuotiTuri() {
    // Ciklas kuris eina per visas figuras masyve
    for(let i = 0; i < figuros.length; i++) {
        // Sukuriu figuros objektui turio rakta (key), ir priskiriu suskaiciuota reiksme
        figuros[i].turis = figuros[i].ilgis * figuros[i].plotis * figuros[i].aukstis;
    }

}


spausdintiButton.addEventListener("click", function() {
    maxFiguraIndex = -1;
    spausdintiDuomenis(false);
});

function spausdintiDuomenis(spausdintiTuri) {

    // Isvalau lenteleje esancius duomenis
    duomenysTable.innerHTML = "";

    // Jei reikia atvaizduoti turi, tai html'e parodau savo turio th elementa
    if(spausdintiTuri == true) {
        turisTh.classList.remove("hide");
    } else {
        turisTh.classList.add("hide");
    }

    // Einu per figuru masyva ir generuoju lentele
    for(let i = 0; i < figuros.length; i++) {
        // Sukuriame tuscia tr elementa
        let row = document.createElement("tr");

        // tikrinu ar einamosios figuros indeksas sutampa su diziausios figuros indeksu
        if(maxFiguraIndex == i) {
            row.classList.add("red");
        }

        
        // Sukuriame tuscia td elementa
        let ilgisTD = document.createElement("td");

        // Priskiriame jo reiksme figuros ilgiui
        ilgisTD.innerText = figuros[i].ilgis;

        let plotisTD = document.createElement("td");
        plotisTD.innerText = figuros[i].plotis;

        let aukstisTD = document.createElement("td");
        aukstisTD.innerText = figuros[i].aukstis;

        // Priskiriame prie row elemento savo sukurtus td elementus
        row.appendChild(ilgisTD);
        row.appendChild(plotisTD);
        row.appendChild(aukstisTD);


        // Patikrinu ar reikia spausdinti turio stulpeli
        if(spausdintiTuri == true) {
            let turisTD = document.createElement("td");
            turisTD.innerText = figuros[i].turis;
            row.appendChild(turisTD);
        }
        

        // Suformuota row elementa priskiriame savo table elementui, kad butu atvaizduojama
       duomenysTable.appendChild(row);
        
    }    
}